import axios from 'axios';
import jwtDecode from 'jwt-decode';
import React from 'react';
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import '../css/Navbar.css';

const Navbar = () => {
    const scrollToAdmin = () => {
        const adminSection = document.getElementById('admin-section');
        if (adminSection) {
            adminSection.scrollIntoView({
                behavior: 'smooth',
                block: 'start',
                inline: 'nearest',
                duration: 2000,
            });
        }
    };
    const { id: userId } = useParams();

    const navigate = useNavigate();
    const isLoggedIn = localStorage.getItem('token') !== null;
    let isAdmin = false;
    let avatarUrl = '';

    if (isLoggedIn) {
        const decodedToken = jwtDecode(localStorage.getItem('token'));
        isAdmin = decodedToken.role === 'admin';
        avatarUrl = decodedToken.foto;
    }

    const handleLogout = async () => {
        Swal.fire({
            title: 'Cerrar sesión',
            text: '¿Estás seguro de cerrar sesión?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, cerrar sesión',
            cancelButtonText: 'Cancelar',
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    await axios.post('https://anacap.host/api/logout');
                    localStorage.removeItem('token');
                    Swal.fire('Sesión cerrada', 'Has cerrado sesión exitosamente.', 'success')
                        .then(() => {
                            setTimeout(() => {
                                window.location.reload();
                            }, 3000);
                        });
                } catch (error) {
                    Swal.fire('Error', 'Ocurrió un error al cerrar sesión.', 'error');
                }
            }
        });
    };

    const isTokenExpired = () => {
        if (!isLoggedIn) {
            return false;
        }

        const decodedToken = jwtDecode(localStorage.getItem('token'));
        const currentTime = Date.now() / 1000;
        return decodedToken.exp < currentTime;
    };


    const handleClickPerfile = () => {
        const decodedToken = jwtDecode(localStorage.getItem('token'));
        const userId = decodedToken.id; 
        navigate(`/perfil/${userId}`);
      };

    return (
        <div className="navbar">
        <div className="logo">
          <a href="/" className={'li'}>
            <h4>Products System</h4>
          </a>
          <a href="/">
            <img src="logo.png" alt="Logo" />
          </a>
        </div>
        <ul className="link">
          <li>
            <NavLink to="/" className={'li'}>
              <i className="fas fa-home"></i> Home
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" className={'li'}>
              <i className="fas fa-users"></i> Quienes Somos
            </NavLink>
          </li>
          <li>
            <NavLink to="/read-products" className={'li'}>
              <i className="fas fa-list"></i> Ver Productos
            </NavLink>
          </li>
          <li>
            <NavLink to="/add-products" className={'li'}>
              <i className="fas fa-address-book"></i> Agregar Producto
            </NavLink>
          </li>
        </ul>
        <ul className="link">
          <li>
            {isLoggedIn ? (
              <>
                <button className={'li boton'} onClick={handleLogout}>
                  <i className="fas fa-sign-out-alt"></i> Logout
                </button>
              </>
            ) : (
              <>
                <ul className="link">
                  <li>
                    <NavLink to="/login" className={'li'}>
                      <i className="fas fa-sign-in-alt"></i> Iniciar Sesion
                    </NavLink>
                  </li>
                  <li>
                    <NavLink to="/register" className={'li'}>
                      <i className="fas fa-user-plus"></i> Registrarse
                    </NavLink>
                  </li>
                </ul>
              </>
            )}
          </li>
        </ul>
        <div id="admin-section"></div>
      </div>
    );
};

export default Navbar;