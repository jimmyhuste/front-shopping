import React from 'react';
import "../css/Footer.css";

function Footer() {
  const year = new Date().getFullYear(); // Get current year

  return (
    <footer className="footer">
      <div className="container">
        
        <div className="copyright">
          <p>&copy; {year} Jimmy Huste Exantus. Todos los derechos reservados.</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
