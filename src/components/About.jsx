import React from 'react';
import "../css/About.css";

const About = () => {
  return (
    <div className="about-container">
      <div className="about-image-container">
        <img src="mision.jpeg" alt="Imagen de la empresa" className="about-image" />
      </div>
      <div className="about-content">
        <div className="about-header">
          <h1 className="about-title">Acerca de Nosotros</h1>
          <p className="about-subtitle">Conoce un poco más sobre nosotros." - Esta frase es genérica y poco informativa. No le dice al lector qué hace única a tu empresa o qué te apasiona. Cumple con la función básica de introducir la sección Acerca de Nosotros, pero carece de la profundidad y el atractivo que pueden captar a clientes potenciales.</p>
        </div>
      </div>
    </div>
  );
};

export default About;
