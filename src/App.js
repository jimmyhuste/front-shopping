import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

import './App.css';
import About from './components/About';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import NotFound from './components/NotFound';
import AddProducts from './pages/AddProducts';
import Home from './pages/Home';
import ReadProducts from './pages/ReadProducts';
import UpdateProducts from './pages/UpdateProducts';
import Login from './pages/auth/Login';
import Register from './pages/auth/Register';

function App() {
  return (
    <div className="App">

      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          {/* VALIDAR QUE SI HAY UN USUARIO Y QUE TIPO DE USUARIO ES PARA DAR O NEGARLE EL ACCESO A LA PAGINA ADMIN. 
          <Route element = { <PrivateRoute canActivate = {freeAuth()}/>}>
            <Route path="/admin" element={<AdminPage />} />
          </Route>  
          */}
        
          <Route path="/about" element={<About />} />
          <Route path="/add-products" element={<AddProducts />} />
          <Route path="/update-product/:id" element={<UpdateProducts />} />
          <Route path="/read-products" element={<ReadProducts />} />
          {/*<Route path="/perfil/:id" element={<EditUser />} />*/}
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Router>

      <Footer />


    </div>
  );
}

export default App;


