import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

const UpdateProducts = () => {
  const [nombre, setNombre] = useState("");
  const [precio, setPrecio] = useState(null);
  const [categoria, setCategoria] = useState({ nombre: "", descripcion: "" });
  const [descuento, setDescuento] = useState("0");
  const [fotografias, setFotografias] = useState([]);
  const [descripcion, setDescripcion] = useState("");
  const [productID, setProductID] = useState(null); // State to store the product ID
  const navigate = useNavigate(); // Use useNavigate for navigation

  const [products, setProducts] = useState([]);
  const [token, setToken] = useState(null); // State to store the authentication token

  const params = useParams(); // Get route parameters
  const { id: productIDFromParams } = params; // Extract product ID from parameters

  
  useEffect(() => {
    // Retrieve token from local storage
    const localStorageToken = localStorage.getItem("token");
    setToken(localStorageToken);
  
    const fetchProduct = async () => {
      try {
        const response = await axios.get(
          `http://localhost:8080/ms-poc/products/${productIDFromParams}`,
          {
            headers: {
              Authorization: `Bearer ${localStorageToken}`,
            },
          }
        );
        const product = response.data;
  
        setNombre(product.nombre);
        setPrecio(product.precio);
        setCategoria({
          nombre: product.categoria.nombre,
          descripcion: product.categoria.descripcion,
        });
        setDescuento(product.descuento.toString());
        setFotografias(product.fotografias);
        setDescripcion(product.descripcion);
        setProductID(product.id);
      } catch (error) {
        console.error("Error fetching product:", error);
        if (error.response && error.response.status === 401) {
          console.warn('Acceso no autorizado. Token might be invalid.');
          window.location.href = '/login'; // Redirigir al usuario al inicio de 
          
        } else {
          console.error('Network error or other issue:', error);
        }
      }
    };
  
    if (localStorageToken && productIDFromParams) {
      fetchProduct();
    }
  }, [productIDFromParams, token])
  const handleSubmit = async (e) => {
    e.preventDefault();

    const errors = [];

// Validate the 'nombre' field
if (!nombre) {
  errors.push('El campo "Nombre" es obligatorio.');
} else if (nombre.trim().length < 3) {
  errors.push('El campo "Nombre" debe tener al menos 3 caracteres.');
} else if (nombre.trim().length > 50) {
  errors.push('El campo "Nombre" no debe tener más de 50 caracteres.');
}

     // Validate the 'Precio' field
     if (!precio) {
      errors.push('El campo "Precio" es obligatorio.');
    } else if (isNaN(precio) || precio <= 0) {
      errors.push('El campo "Precio" debe ser un número decimal positivo.');
    }

    if (descuento < 0 || descuento > 100) {
      errors.push('El campo "Descuento" debe ser un porcentaje entre 0 y 100');
    }

    if (errors.length > 0) {
      Swal.fire({
        icon: "error",
        title: "Errores en el formulario",
        text: errors.join("\n"),
      });
      return;
    }

    try {
      const updatedProduct = {
        id: productID,
        nombre,
        precio,
        categoria: {
          nombre: categoria.nombre,
          descripcion: categoria.descripcion,
        },
        descuento: parseInt(descuento),
        fotografias: [],
        descripcion,
      };

      const response = await axios.put(
        `http://localhost:8080/ms-poc/products/${productIDFromParams}`,
        updatedProduct,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
   
      Swal.fire({
        icon: "success",
        title: "Producto actualizado con éxito",
      });

      // navigate('/productos'); // Redirect to product list page
      navigate(`/update-product/${productID}`); // Redirect to updated product page
    } catch (error) {
      console.error("Error updating product:", error);
      Swal.fire({
        icon: "error",
        title: "Error al actualizar el producto",
        text: error.message, // Corrected line, closing curly brace after message
      });
    }
  };

  
  const handleImageUpload = (event) => {
    const file = event.target.files[0];
  
    if (!file) return; // Handle no file selected case
  
    const reader = new FileReader();
    reader.onloadend = () => {
      // Convert base64 to byte array
      const arrayBuffer = reader.result.split(",")[1]; // Remove data URL prefix
      const byteArray = new Uint8Array(atob(arrayBuffer));
  
      console.log("Image data (byte array):", byteArray);
    };
    reader.readAsDataURL(file);
  };

  return (
    <div>
      <h1>Actualizar Producto</h1>

      <form onSubmit={handleSubmit} className="form-container">
        <div className="form-row">
          <div className="form-column">
            <label>Nombre</label>
            <input
              type="text"
              value={nombre}
              onChange={(e) => setNombre(e.target.value)}
            />

            <label>Precio</label>
            <input
              type="number"
              value={precio}
              onChange={(e) => setPrecio(e.target.valueAsNumber)}
            />

            <label>Categoría</label>
            <select
              value={categoria.nombre}
              onChange={(e) =>
                setCategoria({ ...categoria, nombre: e.target.value })
              }
            >
              <option value="Madera">Madera</option>
              <option value="Electronica">Electrónica</option>
              <option value="Hogar">Hogar</option>
            </select>
          </div>
          <div className="form-column">
            <label>Descuento</label>
            <input
              type="number"
              value={descuento}
              onChange={(e) => setDescuento(e.target.value)}
            />
           <label htmlFor="foto">Foto</label>
            <input
              type="file"
              id="foto"
              name="foto"
              onChange={(event) => {
                handleImageUpload(event);
                setFotografias(event.target.value);
              }}
            />

            <label>Descripción</label>
            <textarea
              value={descripcion}
              onChange={(e) => setDescripcion(e.target.value)}
            />
          </div>
        </div>

        <button type="submit">Actualizar Producto</button>
      </form>
    </div>
  );
};

export default UpdateProducts;
