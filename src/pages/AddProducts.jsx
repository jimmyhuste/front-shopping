import axios from "axios";
import React, { useState } from "react";
import Swal from "sweetalert2";
import "../css/AddProducts.css";

const AddProducts = () => {
  // State variables for product details
  const [nombre, setNombre] = useState("");
  const [precio, setPrecio] = useState(null);
  const [categoria, setCategoria] = useState({
    nombre: "Madera",
    descripcion: "Producto de categoría madera de muy buena calidad.",
  });
  const [descuento, setDescuento] = useState("0");
  const [descripcion, setDescripcion] = useState(
    "Es un producto bueno que deberías comprar."
  );
  const [fotografias, setFotografias] = useState([]);

  // Function to handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    // Initialize an empty array to store errors
    const errors = [];

   // Validate the 'nombre' field
if (!nombre) {
  errors.push('El campo "Nombre" es obligatorio.');
} else if (nombre.trim().length < 3) {
  errors.push('El campo "Nombre" debe tener al menos 3 caracteres.');
} else if (nombre.trim().length > 50) {
  errors.push('El campo "Nombre" no debe tener más de 50 caracteres.');
}

    // Validate the 'Precio' field
    if (!precio) {
      errors.push('El campo "Precio" es obligatorio.');
    } else if (isNaN(precio) || precio <= 0) {
      errors.push('El campo "Precio" debe ser un número decimal positivo.');
    } else if (nombre >=2) {
      errors.push('El campo "Nombre" debe tener mas de 3 caracteres');
    }

    // Validate the 'Descuento' field
    if (descuento < 0 || descuento > 100) {
      errors.push('El campo "Descuento" debe ser un porcentaje entre 0 y 100.');
    }

    // If there are any errors, display them to the user
    if (errors.length > 0) {
      Swal.fire({
        icon: "error",
        title: "Errores en el formulario",
        text: errors.join("\n"),
      });
      return;
    }

    const localStorageToken = localStorage.getItem("token");

    if (!localStorageToken) {
      // Handle the case where the token is not found (e.g., display an error message)
      console.error("No token found in local storage");
      return; // Prevent further execution if there's no token
    }
    
    // Proceed with using the token as usual
    const token = localStorageToken;
    // Try to add the product using axios
    try {
      const response = await axios.post("http://localhost:8080/ms-poc/products",
      {
        nombre,
        precio,
        categoria,
        descuento,
        fotografias,
        descripcion,
      },
      { headers: { Authorization: `Bearer ${token}` } }
    );

      // Display a success message to the user
      Swal.fire({
        icon: "success",
        title: "Producto agregado con éxito",
      });

      // Reset the form fields after a successful submission
      setNombre("");
      setPrecio(null);
      setCategoria({ nombre: "Madera", descripcion: "" });
      setDescuento("0");
      setFotografias("");
      setDescripcion("");
    } catch (error) {
      console.error(error); // Log the error
      if (error.response && error.response.status === 401) {
        console.warn('Acceso no autorizado. Token might be invalid.');
        window.location.href = '/login'; // Redirigir al usuario al inicio de 
        
      } else {
        console.error('Network error or other issue:', error);
      }

      // Display an error message to the user
      Swal.fire({
        icon: "error",
        title: "Error al agregar el producto",
        text: error.message,
      });
    }
  };
  const handleImageUpload = (event) => {
    const files = event.target.files;
    if (!files.length) return;

    const newFotografias = [];
    for (const file of files) {
      const reader = new FileReader();
      reader.onloadend = async () => {
        const arrayBuffer = reader.result.split(",")[1]; // Remove data URL prefix
        const byteArray = new Uint8Array(atob(arrayBuffer));

        const newPhoto = {
          nombre: "Foto producto", // Adjust name as needed
          foto: byteArray, // Byte array representation of the image
          extension: file.name.split(".").pop(), // Get extension from file name
        };

        newFotografias.push(newPhoto);
      };
      reader.readAsDataURL(file);
    }

    setFotografias([...fotografias, ...newFotografias]);
  };

  return (
    <div>
      {localStorage.getItem("token") ? (
  <>
    <h1>Agregar Producto</h1>

    <form onSubmit={handleSubmit}>
      <div className="form-row">
        <div className="form-column">
          <label>Nombre:</label>
          <input
            type="text"
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            required
          />

          <label>Precio:</label>
          <input
            type="number"
            value={precio}
            onChange={(e) => setPrecio(e.target.valueAsNumber)}
            required
            min="0"
            step="0.01" // Allow decimal numbers
          />

          <label>Categoría:</label>
          <select
            value={categoria.nombre}
            onChange={(e) =>
              setCategoria({ ...categoria, nombre: e.target.value })
            }
            required
          >
            <option value="Madera">Madera</option>
            <option value="Electronica">Electrónica</option>
            <option value="Hogar">Hogar</option>
          </select>
        </div>
        <div className="form-column">
          <label>Descuento:</label>
          <input
            type="number"
            value={descuento}
            onChange={(e) => {
              const value = parseInt(e.target.value);
              if (!isNaN(value) && value >= 0 && value <= 100) {
                setDescuento(value);
              }
            }}
            required
          />

            <label htmlFor="foto">Foto</label>
            <input
              type="file"
              id="foto"
              name="foto"
              onChange={(event) => {
                handleImageUpload(event);
                setFotografias(event.target.value);
              }}
            />

          <label>Descripción:</label>
          <textarea
            value={descripcion}
            onChange={(e) => setDescripcion(e.target.value)}
            required
          />
        </div>
      </div>

      <button type="submit">Agregar Producto</button>
    </form>
  </>
) : (
  <p className="no-access">Usted no tiene acceso a esta página. Debes iniciar sesion y vuelve a intentar.</p>
)}

    </div>
  );
};

export default AddProducts;
