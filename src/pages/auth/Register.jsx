import axios from "axios";
import React, { useState } from "react";
import Swal from "sweetalert2";
import "../../css/auth/Register.css";

function Register() {
  const [rut, setRut] = useState("27485940-7");
  const [nombre, setNombre] = useState("Jimmy Huste");
  const [apellidoPaterno, setApellidoPaterno] = useState("Exantus");
  const [apellidoMaterno, setApellidoMaterno] = useState("No aplica");
  const [telefono, setTelefono] = useState("987654321");
  const [email, setEmail] = useState("jimmyhuste@gmail.com");
  const [direccion, setDireccion] = useState("Pudahuel 841, Pudahuel");
  const [foto, setFoto] = useState("");
  const [fechaNacimiento, setFechaNacimiento] = useState("24-12-1994");
  const [sexo, setSexo] = useState("Masculino");
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Validate all required fields (except foto, telefono)
    if (
      !rut ||
      !nombre ||
      !apellidoPaterno ||
      !apellidoMaterno ||
      !email ||
      !direccion ||
      !fechaNacimiento ||
      !sexo ||
      !user ||
      !password
    ) {
      setErrorMessage("Debe completar todos los campos obligatorios"); // Error message in Spanish
      return;
    }

    // Validate email format
    if (
      !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        email
      )
    ) {
      setErrorMessage("Formato de correo electrónico no válido"); // Error message in Spanish
      return;
    }

    // Validate sexo (M or F)
    if (!sexo.toUpperCase().match(/M|F/)) {
      setErrorMessage("Sexo no válido (M o F)"); // Error message in Spanish
      return;
    }

    // Prepare registration data
    const registrationData = {
      rut,
      nombre,
      apellidoPaterno,
      apellidoMaterno,
      telefono,
      email,
      direccion,
      foto,
      fechaNacimiento,
      sexo,
      user,
      password,
    };

    try {
      // Send registration request using Axios
      const response = await axios.post(
        "http://localhost:8080/ms-poc/auth/signup",
        registrationData
      );
      console.log(response.status);

      setRut("");
      setNombre("");
      setApellidoPaterno("");
      setApellidoMaterno("");
      setTelefono("");
      setEmail("");
      setDireccion("");
      setFoto("");
      setFechaNacimiento("");
      setSexo("");
      setUser("");
      setPassword("");
      setErrorMessage("");

      Swal.fire({
        icon: "success",
        title: "Registro Exitoso", // Título de éxito en español
        text: "¡Bienvenido/a, " + nombre + "!", // Mensaje de éxito en español
      });

      console.log("Registro exitoso");
    } catch (error) {
      // Manejar errores de red u otras excepciones
      console.error("Error en el registro:", error);
      setErrorMessage(
        "Error inesperado. Por favor, inténtelo de nuevo más tarde."
      );
    }
  };
  const handleImageUpload = (event) => {
    const file = event.target.files[0];

    if (!file) return; // Handle no file selected case

    const reader = new FileReader();
    reader.onloadend = () => {
      // Convert base64 to byte array
      const arrayBuffer = reader.result.split(",")[1]; // Remove data URL prefix
      const byteArray = new Uint8Array(atob(arrayBuffer));

      console.log("Image data (byte array):", byteArray);
    };
    reader.readAsDataURL(file);
  };

  return (
    <div className="register-container">
      <h2>Register</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-row">
          <div className="form-group">
            <label htmlFor="rut">Rut</label>
            <input
              type="text"
              id="rut"
              name="rut"
              value={rut}
              onChange={(e) => setRut(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="nombre">Nombre</label>
            <input
              type="text"
              id="nombre"
              name="nombre"
              value={nombre}
              onChange={(e) => setNombre(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="apellidoPaterno">Apellido Paterno</label>
            <input
              type="text"
              id="apellidoPaterno"
              name="apellidoPaterno"
              value={apellidoPaterno}
              onChange={(e) => setApellidoPaterno(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="apellidoMaterno">Apellido Materno</label>
            <input
              type="text"
              id="apellidoMaterno"
              name="apellidoMaterno"
              value={apellidoMaterno}
              onChange={(e) => setApellidoMaterno(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="telefono">Teléfono (opcional)</label>
            <input
              type="tel" // Use tel for phone number format
              id="telefono"
              name="telefono"
              value={telefono}
              onChange={(e) => setTelefono(e.target.value)}
            />
          </div>

          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="foto">Foto</label>
            <input
              type="file"
              id="foto"
              name="foto"
              onChange={(event) => {
                handleImageUpload(event);
                setFoto(event.target.value);
              }}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group">
            <label htmlFor="direccion">Dirección</label>
            <input
              type="text"
              id="direccion"
              name="direccion"
              value={direccion}
              onChange={(e) => setDireccion(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="fechaNacimiento">
              Fecha Nacimiento (YYYY-MM-DD)
            </label>
            <input
              type="date" // Use date for date picker
              id="fechaNacimiento"
              name="fechaNacimiento"
              value={fechaNacimiento}
              onChange={(e) => setFechaNacimiento(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="sexo">Sexo (M/F)</label>
            <select
              id="sexo"
              name="sexo"
              value={sexo}
              onChange={(e) => setSexo(e.target.value)}
              required
            >
              <option value="">Selecciona Sexo</option>
              <option value="M">Masculino</option>
              <option value="F">Femenino</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="user">Usuario</label>
            <input
              type="text"
              id="user"
              name="user"
              value={user}
              onChange={(e) => setUser(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Contraseña</label>
            <input
              type="password"
              id="password"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        {errorMessage && <div className="error-message">{errorMessage}</div>}
        <button type="submit">Registrarse</button>
      </form>
    </div>
  );
}

export default Register;
