import React, { useState } from 'react';
import Swal from 'sweetalert2';
import "../../css/auth/Login.css"; // Assuming your CSS file path

function Login() {
  const [user, setuser] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const isButtonDisabled = !user || !password; // Disable button if empty fields

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Validate user and password
    if (!user || !password) {
      setErrorMessage('Please enter both user and password');
      return;
    }

    try {
      // Perform login request to backend endpoint
      const response = await fetch('http://localhost:8080/ms-poc/auth/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user,
          password,
        }),
      });

      const responseData = await response.json();

      // Handle login response
      if (response.ok) {
        // Successful login
        setuser('');
        setPassword('');
        setErrorMessage('');

        // Check for token in response
        if (responseData.token) {
          // Store token in local storage
          localStorage.setItem('token', responseData.token);
          console.log('Token stored:', responseData.token); // For verification
          window.location.href = '/read-products';
          Swal.fire({
            icon: 'success',
            title: 'Login Successful',
            text: 'Welcome back!',
          });
        } else {
          setErrorMessage('Login successful');
          Swal.fire({
            icon: 'warning',
            title: 'Successful',
            text: 'Login successful',
          });
        }
      } else {
        // Invalid credentials or other errors
        setErrorMessage(responseData.message || 'Usuario o Password incorrecto');
        Swal.fire({
          icon: 'error',
          title: 'Login Failed',
          text: errorMessage,
        });
      }
    } catch (error) {
      // Handle network errors or other exceptions
      console.error('Login error:', error);
      setErrorMessage('Algo salió mal. por favor intenta nuevamente.');
      Swal.fire({
        icon: 'error',
        title: 'Error al hacer Login',
        text: errorMessage,
      });
    }
  };

  return (
    <div className="login-container">
      <h2>Login</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="user">user</label>
          <input
            type="text"
            id="user"
            name="user"
            placeholder='my user'
            value={user}
            onChange={(e) => setuser(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            placeholder='my password'
            id="password"
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        {errorMessage && <div className="error-message">{errorMessage}</div>}
        <button type="submit" disabled={isButtonDisabled}>
          Login
        </button>
      </form>
    </div>
  );
}

export default Login;
