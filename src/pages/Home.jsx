import React from 'react';
import "../css/Home.css";

const Home = () => {
  return (
    <div className="home-container">
      <h1 className="home-title">
        ¡Bienvenido a <span className="highlight">nuestra tienda en línea!</span>
      </h1>
      <p className="home-description">
        Encuentra productos de alta calidad a <span className="highlight">precios increíbles.</span>
      </p>
    </div>
  );
};

export default Home;
