import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import "../css/ReadProducts.css";

const ReadProducts = () => {
  const [products, setProducts] = useState([]);
  const [token, setToken] = useState(null); // State to store the authentication token

  useEffect(() => {
    // Retrieve token from local storage
    const localStorageToken = localStorage.getItem('token');
    setToken(localStorageToken);
  
    const fetchProducts = async () => {
      try {
        const response = await axios.get('http://localhost:8080/ms-poc/products', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
  
       
      
        setProducts(response.data); 
  
      } catch (error) {
        console.error('Error fetching products:', error);
  
        if (error.response && error.response.status === 401) {
          console.warn('Acceso no autorizado. Token might be invalid.');
          window.location.href = '/login'; // Redirigir al usuario al inicio de 
          
        } else {
          console.error('Network error or other issue:', error);
        }
      }
    };
  
    // Fetch products if token exists
    if (token) {
      fetchProducts();
    }
  }, [token]); // Re-fetch products on token change
  

  const handleDelete = async (product) => {
    const result = await Swal.fire({
      title: '¿Estás seguro de eliminar este producto?',
      text: 'Esta acción no se puede deshacer.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
    });

    if (result.isConfirmed) {
      try {
        // Implement logic to delete the product
        console.log('Delete product:', product); // Replace with actual deleting logic

        // Update products state after successful deletion
        const updatedProducts = products.filter((p) => p.id !== product.id);
        setProducts(updatedProducts);
     
        // Show success message
        Swal.fire('¡Producto eliminado!', '', 'success');
      } catch (error) {
        // Handle errors during deletion
        console.error('Error al eliminar producto:', error);
        Swal.fire('Error al eliminar producto', error.message, 'error');
      }
    }
  };

  return (
    <div className="container">
    {token ? (
      <>
        <h1 className="text-center">Productos</h1>

        <div className="product-grid">
          {products.length > 0 ? (
            products.map((product) => (
              <div className="product-card" key={product.id}>
                <img src={product.foto} alt={product.nombre} className="product-image" />
                <div className="product-info">
                  <h3 className="product-name">{product.nombre}</h3>
                  <p className="product-price">Precio: ${product.precio}</p>
                  <p className="product-category">Categoría: {product.categoria.nombre}</p>
                  <p className="product-discount">Descuento: {product.descuento}%</p>
                  <p className="product-description">{product.descripcion}</p>

                  <div className="product-actions">
                    <Link to={`/update-product/${product.id}`}>Editar</Link>
                    <button className="delete-button" onClick={() => handleDelete(product)}>Eliminar</button>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <p className="no-products">No hay productos disponibles.</p>
          )}
        </div>
      </>
    ) : (
      <p className="no-access">Usted no tiene acceso a esta página. Debes iniciar sesion y vuelve a intentar.</p>
    )}
  </div>
  );
};

export default ReadProducts;
